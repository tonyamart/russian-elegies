Покинь меня, мой юный друг, ―
Твой взор, твой голос мне опасен: 
Я испытал любви недуг, 
И знаю я, как он ужасен… 
Но что, безумный, я сказал? 
К чему укоры и упреки? 
Уж я твой узник, друг жестокий, 
Твой взор меня очаровал. 
Я увлечен своей судьбою, 
Я сам к погибели бегу: 
Боюся встретиться с тобою, 
А не встречаться не могу. 