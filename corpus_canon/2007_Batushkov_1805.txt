Как счастье медленно приходит,
Как скоро прочь от нас летит!
Блажен, за ним кто не бежит,
Но сам в себе его находит!
В печальной юности моей
Я был счастлив - одну минуту,
Зато, увы! и горесть люту
Терпел от рока и людей!
Обман надежды нам приятен,
Приятен нам хоть и на час!
Блажен, кому надежды глас
В самом несчастьи сердцу внятен!
Но прочь уже теперь бежит
Мечта, что прежде сердцу льстила;
Надежда сердцу изменила,
И вздох за нею вслед летит!
Хочу я часто заблуждаться,
Забыть неверную... но нет!
Несносной правды вижу свет,
И должно мне с мечтой расстаться!
На свете всё я потерял,
Цвет юности моей увял:
Любовь, что счастьем мне мечталась,
Любовь одна во мне осталась!