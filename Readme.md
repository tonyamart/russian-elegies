### Russian elegies corpus

This is a repository that contains the corpus of Russian elegies (19th century, first half) as well as related files such as metadata .csv tables and scripts.

The most recent contributions to the corpus could be found in the folder "corpus in progress"
